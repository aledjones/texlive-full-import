FROM debian:bullseye
ENV DEBIAN_FRONTEND=noninteractive
RUN apt update
RUN apt install -y --no-install-recommends texlive-full
RUN apt install -y --no-install-recommends latexmk
RUN apt clean -y